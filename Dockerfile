FROM perl:5.34-buster

RUN groupadd --gid 1000 perl \
    && useradd --uid 1000 --gid perl --shell /bin/bash --create-home perl

RUN set -ex \
    && apt update && apt install -y --no-install-recommends \
    software-properties-common \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    mariadb-client \
    && rm -rf /var/cache/apt/archives/* \
    && rm -rf /var/lib/apt/lists/*

# Install Docker
RUN curl -fsSL https://download.docker.com/linux/debian/gpg \
    | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg \
    && echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
    | tee /etc/apt/sources.list.d/docker.list > /dev/null \
    && apt update && apt install -y --no-install-recommends \
    docker-ce \
    docker-ce-cli \
    containerd.io \
    && rm -rf /var/cache/apt/archives/*

WORKDIR /app

COPY cpanfile /app
RUN set -ex \
    && cd /app && cpanm --installdeps . \
    && rm -r /var/lib/apt/lists/* \
    && rm -r /root/.cpanm

COPY sandbox_manager /app/sandbox_manager
COPY ansible /app/ansible

COPY run.sh /app

EXPOSE 8000 8000

CMD ["/bin/bash", "/app/run.sh"]
