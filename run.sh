#!/bin/bash

set -e

export sandboxes_dir="/sandboxes"
# Initialize directories in a safe way
mkdir -p "$sandboxes_dir/configs"
mkdir -p "$sandboxes_dir/logs"

# TODO: this could be removed if we go full Docker (i.e. hardcode what we need directly in the files)
# FIXME: It feels like the secrets should be generated the first time and persisted?
cat 1> /app/sandbox_manager/sanbox_manager.conf <<EOM
{
  perldoc => 1,
  secrets => ['1d8d597a1054cb4535b6ed118da21d2c34ca5dfa'],

  hypnotoad => {
    listen   => ["http://*:8000"],
    pid_file => '/app/hypnotoad.pid'
  },
}
EOM

chown -R perl:perl \
    $sandboxes_dir \
    /app

su - perl -c "LOG_TO_STDERR=yes hypnotoad -f /app/sandbox_manager/script/sandbox_manager"
